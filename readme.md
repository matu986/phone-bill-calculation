# Stratio Code Challenge - Phone bill calculation

Matias Nicoletti - https://bitbucket.org/matu986/phone-bill-calculation/

## Design and Development notes
-------

### PhoneBillController:
 * I decided to develop the billing logic as a REST API (as it would probably be in a real scenario).
 * The controller validates the text input (billDetails REQUEST param) and returns **HTTP 400 Bad Request** if the parsing fails (checks regex, delimiters, etc). I used Spring ControllerAdvice support for exception handling.
 * The parsing of the input is handled with the Java API classes Scanner, Pattern and Matcher (Regex handling).
 * The **GET /phone-bill** endpoint returns a JSON with the list of the phone calls (individually priced) and the grand total in cents.
	
### PhoneBillService and  BillingRule:
 * The service layer handles the calculation of prices.
 * The Billing Rules model is extensible (through the BillingRule interface) and configurable through properties (application.yml). You can modify the different values of the current rules (thresholds, unit prices, etc).
 * To add a new Rule, we must create a new implementation of the BillingRule interface and implement the apply method. New rules will be automcatically injected into PhoneBillService (@Autowired collection). To handle the precedence of execution, we use the @Order annotation (e.g., in the example, MostCalledNumberBillingRule should be executed at the end).
	
### UnitTesting
 * Used SpringBoot support for unit testing: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html.
 * Testing at API endpoint level (PhoneBillControllerTests).
 * Testing at Service layer level, billing rules logic (PhoneBillServiceTests, BillingRuleTests).
 * Testing at model level, parsing phone calls (PhoneCallTests).


## Build, Test and Run
------

* **mvn clean install** for executing tests and building.
* **mvn spring-boot:run** for running the API.
* Sample request:
```
curl -X GET \ 'http://localhost:8080/phone-bill?billDetails=00:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090'
{
    "phoneCalls": [
        {
            "inputText": "00:01:07,400-234-090",
            "durationInSeconds": 67,
            "phoneNumber": "400-234-090",
            "totalPriceInCents": 0
        },
        {
            "inputText": "00:05:01,701-080-080",
            "durationInSeconds": 301,
            "phoneNumber": "701-080-080",
            "totalPriceInCents": 900
        },
        {
            "inputText": "00:05:00,400-234-090",
            "durationInSeconds": 300,
            "phoneNumber": "400-234-090",
            "totalPriceInCents": 0
        }
    ],
    "totalInCents": 900
}
```