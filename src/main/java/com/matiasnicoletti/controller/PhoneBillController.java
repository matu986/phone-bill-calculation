package com.matiasnicoletti.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.matiasnicoletti.controller.response.PhoneBillResponse;
import com.matiasnicoletti.exception.PhoneCallParsingException;
import com.matiasnicoletti.model.PhoneCall;
import com.matiasnicoletti.service.PhoneBillService;

@RestController
public class PhoneBillController {

	private static final Pattern PHONE_CALL_DELIMITER_PATTERN = Pattern.compile("\\\\n|\\n|\\R|$");
	private static final Pattern PHONE_CALL_PATTERN_CHECKER = Pattern.compile("(\\d+:\\d{2}:\\d{2}),(.{11})");

	@Autowired
	private PhoneBillService phoneBillService;

	@GetMapping("/phone-bill")
	public PhoneBillResponse getPhoneBill(@RequestParam String billDetails) {
		// parse phone calls
		List<PhoneCall> phoneCalls = PHONE_CALL_DELIMITER_PATTERN.splitAsStream(billDetails)
				.peek(singlePhoneCallInput -> validateInput(singlePhoneCallInput)).map(PhoneCall::new)
				.collect(Collectors.toList());
		// execute billing rules
		phoneBillService.computePrices(phoneCalls);
		// calculate sum
		BigDecimal totalPrice = phoneCalls.stream().map(PhoneCall::getTotalPriceInCents).reduce(BigDecimal.ZERO,
				BigDecimal::add);
		// return response object
		return new PhoneBillResponse(phoneCalls, totalPrice);
	}

	private void validateInput(String singlePhoneCallInput) {
		if (!PHONE_CALL_PATTERN_CHECKER.matcher(singlePhoneCallInput).matches()) {
			throw new PhoneCallParsingException("Error while parsing input text: " + singlePhoneCallInput
					+ ". Single phone call does not match pattern: " + PHONE_CALL_PATTERN_CHECKER.pattern());
		}

	}

}
