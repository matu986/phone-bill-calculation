package com.matiasnicoletti.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.matiasnicoletti.controller.response.ErrorResponse;
import com.matiasnicoletti.exception.PhoneCallParsingException;

@ControllerAdvice
public class PhoneBillControllerAdvice extends ResponseEntityExceptionHandler {

	@ResponseBody
	@ExceptionHandler(PhoneCallParsingException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ErrorResponse phoneCallParsingExceptionHandler(PhoneCallParsingException ex) {
		return new ErrorResponse(ex.getMessage());
	}

}
