package com.matiasnicoletti.controller.response;

import java.math.BigDecimal;
import java.util.List;

import com.matiasnicoletti.model.PhoneCall;

public class PhoneBillResponse {

	private List<PhoneCall> phoneCalls;
	private BigDecimal totalInCents;

	public PhoneBillResponse(List<PhoneCall> phoneCalls, BigDecimal totalInCents) {
		super();
		this.phoneCalls = phoneCalls;
		this.totalInCents = totalInCents;
	}

	public List<PhoneCall> getPhoneCalls() {
		return phoneCalls;
	}

	public void setPhoneCalls(List<PhoneCall> phoneCalls) {
		this.phoneCalls = phoneCalls;
	}

	public BigDecimal getTotalInCents() {
		return totalInCents;
	}

	public void setTotalInCents(BigDecimal totalInCents) {
		this.totalInCents = totalInCents;
	}

}
