package com.matiasnicoletti.exception;

public class PhoneCallParsingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PhoneCallParsingException(String string) {
		super(string);
	}

}
