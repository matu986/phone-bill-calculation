package com.matiasnicoletti.model;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.matiasnicoletti.exception.PhoneCallParsingException;

public class PhoneCall {

	private static final Pattern PHONE_CALL_COMPONENTS_DELIMITER = Pattern.compile(",");
	private static final Pattern DURATION_COMPONENTS_DELIMITER = Pattern.compile(":");

	private String inputText;
	private Long durationInSeconds;
	private String phoneNumber;
	private BigDecimal totalPriceInCents;

	public PhoneCall(String inputText) {
		this.inputText = inputText;
		this.totalPriceInCents = BigDecimal.ZERO;
		// parse whole phone call -> Duration + Phone Number
		try (Scanner phoneCallScanner = new Scanner(inputText)) {
			phoneCallScanner.useDelimiter(PHONE_CALL_COMPONENTS_DELIMITER);
			// parse duration -> HH : MM : SS
			try (Scanner durationScanner = new Scanner(phoneCallScanner.next())) {
				durationScanner.useDelimiter(DURATION_COMPONENTS_DELIMITER);
				Integer hours = durationScanner.nextInt();
				Integer mins = durationScanner.nextInt();
				Integer secs = durationScanner.nextInt();
				this.durationInSeconds = Duration.ofHours(hours).getSeconds() + Duration.ofMinutes(mins).getSeconds()
						+ secs;
			}
			this.phoneNumber = phoneCallScanner.next();
		} catch (Exception e) {
			throw new PhoneCallParsingException("Error while parsing input text: " + inputText + ". Exception: "
					+ e.getClass().toString() + ". Message: " + e.getMessage());
		}
	}

	public PhoneCall(String inputText, Long durationInSeconds, String phoneNumber, BigDecimal totalPriceInCents) {
		super();
		this.inputText = inputText;
		this.durationInSeconds = durationInSeconds;
		this.phoneNumber = phoneNumber;
		this.totalPriceInCents = totalPriceInCents;
	}

	public String getInputText() {
		return inputText;
	}

	public void setInputText(String inputText) {
		this.inputText = inputText;
	}

	public Long getDurationInSeconds() {
		return durationInSeconds;
	}

	public void setDurationInSeconds(Long durationInSeconds) {
		this.durationInSeconds = durationInSeconds;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public BigDecimal getTotalPriceInCents() {
		return totalPriceInCents;
	}

	public void setTotalPriceInCents(BigDecimal totalPriceInCents) {
		this.totalPriceInCents = totalPriceInCents;
	}

}
