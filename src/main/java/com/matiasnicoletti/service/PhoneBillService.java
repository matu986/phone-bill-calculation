package com.matiasnicoletti.service;

import java.util.List;

import com.matiasnicoletti.model.PhoneCall;

public interface PhoneBillService {

	void computePrices(List<PhoneCall> phoneCalls);

}
