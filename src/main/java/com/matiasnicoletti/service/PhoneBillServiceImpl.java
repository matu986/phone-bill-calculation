package com.matiasnicoletti.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.matiasnicoletti.model.PhoneCall;
import com.matiasnicoletti.service.rules.BillingRule;

@Component
public class PhoneBillServiceImpl implements PhoneBillService {

	@Autowired
	List<BillingRule> billingRules;

	@Override
	public void computePrices(List<PhoneCall> phoneCalls) {
		billingRules.forEach(rule -> rule.apply(phoneCalls));
	}

}
