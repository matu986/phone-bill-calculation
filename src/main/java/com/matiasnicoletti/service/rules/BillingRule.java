package com.matiasnicoletti.service.rules;

import java.util.List;

import com.matiasnicoletti.model.PhoneCall;

public interface BillingRule {

	void apply(List<PhoneCall> phoneCalls);

}
