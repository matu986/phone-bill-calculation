package com.matiasnicoletti.service.rules;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.matiasnicoletti.model.PhoneCall;

/**
 * If the call was shorter than 5 minutes, then you pay 3 cents for every
 * started second of the call (e.g. for duration “00:01:07” you pay 67 * 3 = 201
 * cents). If the call was at least 5 minutes long, then you pay 150 cents for
 * every started minute of the call (e.g. for duration “00:05:00” you pay 5 *
 * 150 = 750 cents and for duration “00:05:01” you pay 6 * 150 = 900 cents).
 */

@Component
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class CallDurationBillingRule implements BillingRule {

	private static final BigDecimal SECONDS_PER_MINUTE = BigDecimal.valueOf(Duration.ofMinutes(1).getSeconds());
	private static final Integer DEFAULT_SCALE = 0;
	private static final Integer DEFAULT_ROUNDING = BigDecimal.ROUND_CEILING;

	@Value("${billingRules.callDuration.thresholdInSeconds:300}")
	private Integer thresholdInSeconds;

	@Value("${billingRules.callDuration.shortDuration.costBySecondInCents:3}")
	private BigDecimal shortCallCostBySecondInCents;

	@Value("${billingRules.callDuration.longDuration.costByMinuteInCents:150}")
	private BigDecimal longCallCostByMinuteInCents;

	@Override
	public void apply(List<PhoneCall> phoneCalls) {
		phoneCalls.forEach(phoneCall -> {
			BigDecimal durationInSeconds = BigDecimal.valueOf(phoneCall.getDurationInSeconds());
			if (phoneCall.getDurationInSeconds() < thresholdInSeconds) {
				phoneCall.setTotalPriceInCents(shortCallCostBySecondInCents.multiply(durationInSeconds));
			} else {
				// minutes rounded with ceiling strategy (e.g., 05:01 = 6 mins)
				BigDecimal minutes = durationInSeconds.divide(SECONDS_PER_MINUTE, DEFAULT_SCALE, DEFAULT_ROUNDING);
				phoneCall.setTotalPriceInCents(longCallCostByMinuteInCents.multiply(minutes));
			}
		});
	}

}
