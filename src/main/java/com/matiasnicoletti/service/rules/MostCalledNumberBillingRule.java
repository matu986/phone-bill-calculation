package com.matiasnicoletti.service.rules;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.matiasnicoletti.model.PhoneCall;

/**
 * All calls to the phone number that has the longest total duration of calls
 * are free. In the case of a tie, if more than one phone number shares the
 * longest total duration, the promotion is applied only to the phone number
 * whose numerical value is the smallest among these phone numbers.
 */

@Component
@Order(value = Ordered.LOWEST_PRECEDENCE)
public class MostCalledNumberBillingRule implements BillingRule {

	@Value("${billingRules.mostCalledNumber.finalCostPerCall:0}")
	private BigDecimal finalCostPerCall;

	@Override
	public void apply(List<PhoneCall> phoneCalls) {
		// count total duration by number
		Map<String, Integer> totalDurationByNumber = new HashMap<>();
		phoneCalls.stream().forEach(phoneCall -> totalDurationByNumber.merge(phoneCall.getPhoneNumber(),
				phoneCall.getDurationInSeconds().intValue(), (oldValue, one) -> oldValue + one));

		// find phone number with max total duration
		totalDurationByNumber.entrySet().stream().max((entry1, entry2) -> {
			int durationCompare = (entry1.getValue().compareTo(entry2.getValue()));
			// if equal, compare by phone number
			return (durationCompare == 0) ? entry1.getKey().compareTo(entry1.getKey()) : durationCompare;
		}).ifPresent(mostCalledNumberEntry -> {
			// set cost to most called number
			phoneCalls.stream().filter(phoneCall -> phoneCall.getPhoneNumber().equals(mostCalledNumberEntry.getKey()))
					.forEach(phoneCall -> phoneCall.setTotalPriceInCents(finalCostPerCall));
		});
	}

}
