package com.matiasnicoletti.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.matiasnicoletti.service.PhoneBillService;

@RunWith(SpringRunner.class)
@WebMvcTest(PhoneBillController.class)
public class PhoneBillControllerTests {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private PhoneBillService phoneBillService;

	@Test
	public void testGetPhoneBill_OK_3DigitHour() throws Exception {
		Mockito.doNothing().when(phoneBillService).computePrices(Mockito.any());

		mvc.perform(get("/phone-bill?billDetails=00:01:07,400-234-090\n100:05:01,701-080-080\n100:05:00,400-234-090"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.totalInCents").isNumber())
				.andExpect(jsonPath("$.phoneCalls").isArray());

	}

	@Test
	public void testGetPhoneBill_OK_challengeExample() throws Exception {
		Mockito.doNothing().when(phoneBillService).computePrices(Mockito.any());

		mvc.perform(get("/phone-bill?billDetails=00:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090"))
				.andExpect(status().isOk()).andExpect(jsonPath("$.totalInCents").isNumber())
				.andExpect(jsonPath("$.phoneCalls").isArray());

	}

	@Test
	public void testGetPhoneBill_KOBadDelimiter() throws Exception {
		mvc.perform(get("/phone-bill?billDetails=00:01:07,400-234-090----100:05:01,701-080-080"))
				.andExpect(status().is(400));
	}

	@Test
	public void testGetPhoneBill_KOPatternDelimiters() throws Exception {
		mvc.perform(get("/phone-bill?billDetails=00-01-07,400-234-090")).andExpect(status().is(400));
	}

	@Test
	public void testGetPhoneBill_KOPatternString() throws Exception {
		mvc.perform(get("/phone-bill?billDetails=ERROR")).andExpect(status().is(400));
	}

	@Test
	public void testGetPhoneBill_KORequiredParam() throws Exception {
		mvc.perform(get("/phone-bill")).andExpect(status().is(400));
	}

}
