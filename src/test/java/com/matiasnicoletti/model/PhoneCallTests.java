package com.matiasnicoletti.model;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.matiasnicoletti.exception.PhoneCallParsingException;

public class PhoneCallTests {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPhoneCallParsingOK() {
		String inputText = "100:01:01,400-234-090";
		PhoneCall phoneCall = new PhoneCall(inputText);

		assertTrue(phoneCall.getInputText().equals(inputText));
		assertTrue(phoneCall.getDurationInSeconds() == ((100 * 3600) + 60 + 1));
		assertTrue(phoneCall.getPhoneNumber().equals("400-234-090"));
		assertTrue(phoneCall.getTotalPriceInCents().compareTo(BigDecimal.ZERO) == 0);

	}

	@Test(expected = PhoneCallParsingException.class)
	public void testPhoneCallParsingKO_Delimiter() {
		String inputText = "00:01:07-400-234-090";
		PhoneCall phoneCall = new PhoneCall(inputText);
	}

	@Test(expected = PhoneCallParsingException.class)
	public void testPhoneCallParsingKO_Duration() {
		String inputText = "00-01-07,400-234-090";
		PhoneCall phoneCall = new PhoneCall(inputText);
	}

}
