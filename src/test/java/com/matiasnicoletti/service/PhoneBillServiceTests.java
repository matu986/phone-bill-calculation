package com.matiasnicoletti.service;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.matiasnicoletti.PhoneBillCalculationApplication;
import com.matiasnicoletti.model.PhoneCall;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PhoneBillCalculationApplication.class)
public class PhoneBillServiceTests {

	@Autowired
	private PhoneBillService phoneBillService;

	@Before
	public void setUp() {
	}

	@Test
	public void testComputePrices() {
		List<PhoneCall> phoneCalls = new ArrayList<>();
		phoneCalls.add(new PhoneCall("00:01:07,400-234-090", 67L, "400-234-090", BigDecimal.ZERO));
		phoneCalls.add(new PhoneCall("00:05:01,701-080-080", 301L, "701-080-080", BigDecimal.ZERO));
		phoneCalls.add(new PhoneCall("00:05:00,400-234-090", 300L, "400-234-090", BigDecimal.ZERO));

		phoneBillService.computePrices(phoneCalls);

		assertTrue(phoneCalls.size() == 3);
		assertTrue(phoneCalls.get(0).getTotalPriceInCents().compareTo(BigDecimal.ZERO) == 0);
		assertTrue(phoneCalls.get(1).getTotalPriceInCents().compareTo(new BigDecimal("900")) == 0);
		assertTrue(phoneCalls.get(2).getTotalPriceInCents().compareTo(BigDecimal.ZERO) == 0);
	}

}
